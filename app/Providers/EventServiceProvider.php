<?php

namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Contact\Email' => [
            'App\Listeners\Contact\Email'
        ],
        'App\Events\Contact\IM' => [
            'App\Listeners\Contact\IM '
        ]
    ];
}
