<?php
namespace App\Http\Controllers;

#use App\Events\Contact;
use App\Models\Person as Person;
use App\Models\Message as Message;

use Illuminate\Http\Request as Request;
use Illuminate\Http\Response as Response;

use Events\Contact\Email as Email;

use Illuminate\Support\Facades\Log as Log;

class Contact extends Controller {

  	/**
  	* createEvent
  	* Instantiates Contact\Event object from request object
  	* properties
  	* @param Request $request
  	* $contact_type: [email,IM, ... future expansion here]
  	* @param String $contact_type
  	* @param int $to_person_id
    * @return Response
    	*/
    	public function createEvent(Request $request,
    								              String $contact_type,
    								              int $to_person_id)
    	{
      		//Creating an anonymous/temporary Person object
      		//for a one-way contact event
          Log::debug(var_dump($request));
          try {

        		$from_person = Person::newFromRequest($request);
        		$to_person = Person::newFromId($to_person_id);

            if(!$to_person->mailformAllowed()) {
              throw(ValidationException);
            }

            $message = new Message($request->input('message'));

            $email_event = new Email($from_person, $to_person, $message);
        		// Fire off contact event, which then will send e-mail?
          } catch (Exception $e) {
              return response()->json([
                'success' => false,
                'errors' => [$e]
             ]);
          }

          return response()
                ->json(['success' => true ]);
      	}
  }
