CREATE DATABASE dealerinspire_challenge;

use dealerinspire_challenge;

CREATE OR REPLACE TABLE `person` (
`id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
`email` varchar(255) NOT NULL,
`full_name` varchar(255) NOT NULL,
`phone` char(10) DEFAULT NULL,
`allow_mailform` int(1) NOT NULL DEFAULT 0,
`last_client_ip` varchar(64),
`created_at` DATETIME NOT NULL,
`updated_at` DATETIME NOT NULL,
KEY `idx_contact_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

insert into person(id,email,full_name,allow_mailform,created_at,updated_at) values(1,'guy-smiley@example.com','Guy Smiley',1,now(),now());


/*
* mailagent_queue
* Used by Listeners/MailAgent
*/
CREATE OR REPLACE TABLE `mailagent_queue` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`to_email` varchar(255) NOT NULL,
	`from_email` varchar(255) NOT NULL,
	`client_ipv6` ipv6,
	`client_ipv4` ipv4,
	`delivery_error` varchar(512),
	'delivery_status'varchar(8) DEFAULT 'WAITING',
	`message_body`	LONGTEXT NOT NULL,
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
		PRIMARY KEY (`id`),
		KEY `idx_mailagent_q_created_at` (`created_at`),
		KEY `idx_mailagent_q_ipv4` (`client_ipv4`),
		KEY `idx_mailagent_q_ipv6` (`client_ipv6`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE OR REPLACE TABLE `event_log` (
	`id` bigint NOT NULL AUTO_INCREMENT,
	`type` VARCHAR(64) NOT NULL DEFAULT 'BaseEvent',
	`data` JSON NOT NULL DEFAULT '{}',
	`created_at` DATETIME NOT NULL,
	`updated_at` DATETIME NOT NULL,
		PRIMARY KEY (`id`),
		KEY `idx_event_log_created_at` (`created_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
