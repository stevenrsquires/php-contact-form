<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
   return redirect('/index.html');
});

/*
* post => /contact/{type}/{id}/
* Creates a contact event which for message of {type} to contact with
* contact.id == {id}.
* Hinted to take Request in (JSON data)
*/
$router->post('/contact/{type}/{id}', 'Contact@createEvent');
